# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-mobile-vikings

CONFIG += sailfishapp

SOURCES += \
    src/mobilevikingconnection.cpp \
    src/simcard.cpp \
    src/mvsettings.cpp \
    src/xauth/o1.cpp \
    src/xauth/o1requestor.cpp \
    src/xauth/o2settingsstore.cpp \
    src/xauth/simplecrypt.cpp \
    src/harbour-mobile-vikings.cpp \
    src/xauth/o2replyserver.cpp \
    src/xauth/oxmobilevikings.cpp \
    src/usage.cpp \
    src/quantity.cpp \
    src/simbalance.cpp

OTHER_FILES += \
    qml/cover/CoverPage.qml \
    translations/*.ts \
    qml/pages/HomePage.qml \
    qml/pages/AmountState.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/ConnectingPage.qml \
    qml/harbour-mobile-vikings.qml \
    harbour-mobile-vikings.desktop \
    harbour-mobile-vikings.png \
    rpm/harbour-mobile-vikings.changes.in \
    rpm/harbour-mobile-vikings.spec \
    rpm/harbour-mobile-vikings.yaml \
    qml/pages/CoverAmountState.qml \
    qml/cover/CoverBusyIndicator.qml \
    qml/pages/UsagePage.qml \
    qml/pages/DateChoosingDialog.qml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/harbour-mobile-vikings-fr.ts

HEADERS += \
    src/mobilevikingconnection.h \
    src/simcard.h \
    src/mvsettings.h \
    src/xauth/o1.h \
    src/xauth/o1requestor.h \
    src/xauth/o2abstractstore.h \
    src/xauth/o2globals.h \
    src/xauth/o2settingsstore.h \
    src/xauth/simplecrypt.h \
    src/xauth/o2replyserver.h \
    src/xauth/oxmobilevikings.h \
    src/usage.h \
    src/quantity.h \
    src/simbalance.h

QT += quick qml

target.path = /usr/bin

qml.files = qml
qml.path = /usr/share/$${TARGET}

desktop.files = $${TARGET}.desktop
desktop.path = /usr/share/applications

icon.files = $${TARGET}.png
icon.path = /usr/share/icons/hicolor/86x86/apps

INSTALLS += target qml desktop icon

CONFIG += link_pkgconfig
PKGCONFIG += sailfishapp
INCLUDEPATH += /usr/include/sailfishapp

OTHER_FILES += $$files(rpm/*)

RESOURCES += \
    pictures.qrc

QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG += -O0


