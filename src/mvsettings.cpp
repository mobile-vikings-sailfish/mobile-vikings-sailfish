#include "mvsettings.h"
#include "simcard.h"

#include <QSettings>
#include <QDir>
#include <QStandardPaths>
#include <QCoreApplication>
#include <QDebug>

MVSettings::MVSettings(QObject *parent) :
    QSettings(QDir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation))
              .filePath(QCoreApplication::applicationName()) + "/settings.conf", QSettings::NativeFormat, parent)
{
    setPassword("");
    setUserName("");
}

MVSettings::~MVSettings()
{
}

QList<SimCard*> MVSettings::simCards() const
{
    QList<SimCard*> ret;
    foreach (QVariant elem, value("settings/simCards").toList())
    {
        SimCard *sim = new SimCard(elem.toStringList()[0]);
        sim->setAlias(elem.toStringList()[1]);
        ret << sim;
    }

    return ret;
}

void MVSettings::setSimCards(QList<SimCard*> simCards)
{
    QVariantList val;
    foreach (SimCard *card, simCards) {
        QStringList lst;
        lst << card->msisdn() << card->alias();

        val << lst;
    }

    setValue("settings/simCards", val);
}

void MVSettings::setPassword(const QString &password)
{
    setValue("login/password", password.toUtf8().toBase64());
}

void MVSettings::setUserName(const QString &userName)
{
    setValue("login/userName", userName);
}

void MVSettings::setSettingsVersion(const QString &version)
{
    setValue("settings/version", version);
}

void MVSettings::setSelectedSim(int selectedSim)
{
    qDebug() << "Setting selected sim: " << selectedSim;
    setValue("settings/selectedSim", selectedSim);
}

QString MVSettings::password() const
{
    return QByteArray::fromBase64(value("login/password").toByteArray());
}

QString MVSettings::settingsVersion() const
{
    return value("settings/version", "0.0").toString();
}

int MVSettings::selectedSim() const
{
    return value("settings/selectedSim", -1).toInt();
}

QString MVSettings::userName() const
{
    return value("login/userName").toString();
}
