#include "quantity.h"


SimCard::TrafficType Quantity::typeId() const
{
    return m_typeId;
}

void Quantity::setTypeId(const SimCard::TrafficType &typeId)
{
    m_typeId = typeId;
}

double Quantity::amount() const
{
    return m_amount;
}

void Quantity::setAmount(double amount)
{
    m_amount = amount;
    emit amountChanged();
}

double Quantity::assigned() const
{
    return m_assigned;
}

void Quantity::setAssigned(double assigned)
{
    m_assigned = assigned;
    emit assignedChanged();
}

QString Quantity::type() const
{
    return m_type;
}

void Quantity::setType(const QString &type)
{
    m_type = type;
}

double Quantity::progress() const
{
    return amount()/assigned();
}
