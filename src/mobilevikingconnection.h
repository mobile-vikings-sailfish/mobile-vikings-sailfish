#ifndef MOBILEVIKINGCONNECTION_H
#define MOBILEVIKINGCONNECTION_H

#include "simcard.h"
#include "mvsettings.h"
#include "xauth/oxmobilevikings.h"

#include <exception>

#include <QObject>
#include <QDebug>
#include <QDateTime>
#include <QNetworkAccessManager>
#include <QQmlListProperty>
#include <QTimer>

class MobileVikingConnection : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(int selectedSim READ selectedSim WRITE setSelectedSim NOTIFY selectedSimChanged)
    Q_PROPERTY(QQmlListProperty<SimCard> simCards READ simCards NOTIFY simCardsChanged)
    Q_PROPERTY(bool updating READ updating NOTIFY updatingChanged)
    Q_PROPERTY(QString errorInfo READ errorInfo NOTIFY errorInfoChanged)
    Q_PROPERTY(bool linked READ linked NOTIFY linkedChanged)


    enum AuthenticationType {BASIC, OAUTH};

    explicit MobileVikingConnection(AuthenticationType type = OAUTH, QObject *parent = 0);
    ~MobileVikingConnection();

    void setUserName(const QString& username) {
        m_userName = username;
        emit userNameChanged();
    }

    QString userName() const {
        return m_userName;
    }

    void setPassword(const QString& password) {
        m_password = password;
        emit passwordChanged();
    }

    QString password() const {
        return m_password;
    }

    QQmlListProperty<SimCard> simCards() { return QQmlListProperty<SimCard>(this, m_sims); }

    int selectedSim() const;
    void setSelectedSim(int selectedSim);

    bool updating() const;
    void setUpdating(bool updating);

    QString errorInfo() const;
    void setErrorInfo(const QString &errorInfo);

    bool linked() const { return m_oxmv->linked(); }

signals:
    void simCardsReady();

    void userNameChanged();
    void passwordChanged();
    void connected();
    void hostError();
    void loginError();
    void selectedSimChanged();
    void updatingChanged();
    void errorInfoChanged();
    void simCardsChanged();
    void linkedChanged();

public slots:
    void connect();

    void getSimCards();
    void getSimCards(bool alias);
    void getPricePlanDetails(SimCard*);
    void getSimBalance(SimCard*);

    void getTopUpHistory(SimCard* sim, const QDateTime& from, const QDateTime& until, int pageSize = 25, int pageNumber = 1);
    void getUsageHistory(SimCard* sim, const QDateTime& from, const QDateTime& until, int pageSize = 25, int pageNumber = 1, bool addPricePlan = false);

    void update();
    void unlink();

    void updateUsage(const QDate& date = QDate());

private slots:
    void receivedReply(QNetworkReply*);
    void timeoutSlot();

private:
    enum RequestType {
        MSISDN_REQUEST,
        PRICE_PLAN_REQUEST,
        SIM_BALANCE_REQUEST,
        TOPUP_HISTORY_REQUEST,
        USAGE_REQUEST,
        SIMCARD_INFO_REQUEST
    };

    AuthenticationType m_type;

    QString m_authorizationString;
    QString m_userName;
    QString m_password;
    QString m_errorInfo;

    QNetworkAccessManager m_networkManager;
    QNetworkReply *m_networkReply;
    QList<SimCard*> m_sims;
    int m_simsLeft;
    int m_selectedSim;

    MVSettings m_settings;

    bool m_updating;

    QTimer m_timeout;

    OXMobileVikings *m_oxmv;

    QDate m_currentDate;

    bool m_gettingUsage;
    unsigned int m_page;

    bool m_sendGetRequest(RequestType reqType, const QHash<QString, QString>& options = QHash<QString, QString>());
};

class UnsupportedAuthenticationMechanism : public std::exception
{
public:
    UnsupportedAuthenticationMechanism(MobileVikingConnection::AuthenticationType type) : m_type(type)
    {}

    MobileVikingConnection::AuthenticationType type() { return m_type; }

private:
    MobileVikingConnection::AuthenticationType m_type;
};

#endif // MOBILEVIKINGCONNECTION_H
