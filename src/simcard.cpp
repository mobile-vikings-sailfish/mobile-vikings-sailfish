#include "simcard.h"
#include "simbalance.h"
#include "usage.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <QTimer>

SimCard::~SimCard()
{
}

QList<SimCard*> SimCard::fromData(const QByteArray& data)
{
    QList<SimCard*> ret;
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data, &error);
    if (!doc.isArray())
        return ret;

    QJsonArray array = doc.array();

    for(int i = 0; i < array.count(); i++)
    {
        SimCard *sim = 0;

        if (array.at(i).isString())
            sim = new SimCard(array.at(i).toString());

        if (array.at(i).isObject())
        {
            sim = new SimCard(array.at(i).toObject().value("msisdn").toString().replace("+", ""));
            sim->setAlias(array.at(i).toObject().value("alias").toString());
        }

        ret << sim;
    }

    return ret;
}

void SimCard::setInfoFromData(const QByteArray& data)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data, &error);
    if (!doc.isObject())
        return;

    QJsonObject obj = doc.object();

    m_cardNumber = obj.value("cardnumber").toString().replace("+", "");
    m_pin1 = obj.value("pin1").toString();
    m_pin2 = obj.value("pin2").toString();
    m_puk1 = obj.value("puk1").toString();
    m_puk2 = obj.value("puk2").toString();
    m_imsi = obj.value("imsi").toString();
}

void SimCard::setBalanceFromData(const QByteArray &data)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data, &error);
    if (!doc.isObject())
        return;

    QJsonObject obj = doc.object();

    if (!m_balance)
        m_balance = new SimBalance(this);

    m_balance->expired = obj.value("is_expired").toBool();
    m_balance->pricePlan = obj.value("price_plan").toString("Unknown Price Plan");
    m_balance->validity = QDateTime::fromString(obj.value("valid_until").toString(), "yyyy-MM-dd hh:mm:ss");
    m_balance->credits = obj.value("credits").toString();
    m_balance->latestJson = data;

    QJsonArray bundles = obj.value("bundles").toArray();
    QList<Quantity*> quantities;

    for (int i = 0; i < bundles.count(); i++)
    {
        QJsonObject bundle = bundles.at(i).toObject();
        Quantity *quantity = new Quantity(bundle.value("type").toString(),
                                          typeStringtoTypeId(bundle.value("type").toString()),
                                          bundle.value("value").toDouble(),
                                          QDateTime::fromString(bundle.value("valid_from").toString(), "yyyy-MM-dd hh:mm:ss"),
                                          m_balance);
        quantity->setAssigned(bundle.value("assigned").toDouble());

        quantities << quantity;
    }

    m_balance->setBundles(quantities);

    emit balanceChanged();
}

unsigned int SimCard::setUsageFromData(const QByteArray &data)
{
    foreach (Usage *usage, m_usageEntries) {
        usage->deleteLater();
    }

    m_usageEntries.clear();

    return addUsageFromData(data);
}

unsigned int SimCard::addUsageFromData(const QByteArray &data)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data, &error);
    if (!doc.isArray())
        return 0;

    QJsonArray array = doc.array();
    for (int i = 0; i < array.count(); i++)
    {
        QJsonObject entry = array.at(i).toObject();

        Usage *usageEntry = new Usage(this);
        usageEntry->setBalance(entry.value("balance").toString("0.0").toDouble());
        usageEntry->setDurationCall((int)entry.value("duration_call").toDouble());
        usageEntry->setDurationConnection((int)entry.value("duration_connection").toDouble());
        usageEntry->setDurationPretty(entry.value("duration_human").toString());
        usageEntry->setPrice(entry.value("price").toString("0.0").toDouble());
        usageEntry->setTo(entry.value("to").toString(""));
        usageEntry->setDirection(entry.value("is_incoming").toBool() ? Usage::DIRECTION_FROM : Usage::DIRECTION_TO);
        QDateTime t = QDateTime::fromString(entry.value("start_timestamp").toString(""), "yyyy-MM-dd hh:mm:ss");
        t = t.toLocalTime();

        usageEntry->setStart(t);

        if (entry.value("is_data").toBool(false))
            usageEntry->setTrafficType(DATA_TRAFFIC);
        else if (entry.value("is_sms").toBool(false) && entry.value("is_super_on_net").toBool(false))
            usageEntry->setTrafficType(SMS_MV_TRAFFIC);
        else if (entry.value("is_sms").toBool(false))
            usageEntry->setTrafficType(SMS_TRAFFIC);
        else if (entry.value("is_voice").toBool(false) && entry.value("is_super_on_net").toBool(false))
            usageEntry->setTrafficType(VOICE_MV_TRAFFIC);
        else if (entry.value("is_voice").toBool(false))
            usageEntry->setTrafficType(VOICE_TRAFFIC);

        m_usageEntries << usageEntry;
    }

    emit usageChanged();

    return array.count();
}

SimCard::TrafficType SimCard::typeStringtoTypeId(const QString& typeString) const
{
    if (typeString == "voice_super_on_net")
        return VOICE_MV_TRAFFIC;
    else if (typeString == "sms_super_on_net")
        return SMS_MV_TRAFFIC;
    else if (typeString == "sms")
        return SMS_TRAFFIC;
    else if (typeString == "data")
        return DATA_TRAFFIC;

    return UNKNOWN_TRAFFIC;
}

QString SimCard::imsi() const
{
    return m_imsi;
}

void SimCard::setImsi(const QString &imsi)
{
    m_imsi = imsi;
}
QString SimCard::puk2() const
{
    return m_puk2;
}

void SimCard::setPuk2(const QString &puk2)
{
    m_puk2 = puk2;
}
QString SimCard::puk1() const
{
    return m_puk1;
}

void SimCard::setPuk1(const QString &puk1)
{
    m_puk1 = puk1;
}
QString SimCard::pin2() const
{
    return m_pin2;
}

void SimCard::setPin2(const QString &pin2)
{
    m_pin2 = pin2;
}
QString SimCard::pin1() const
{
    return m_pin1;
}

void SimCard::setPin1(const QString &pin1)
{
    m_pin1 = pin1;
}
QString SimCard::cardNumber() const
{
    return m_cardNumber;
}

void SimCard::setCardNumber(const QString &cardNumber)
{
    m_cardNumber = cardNumber;
}
QString SimCard::alias() const
{
    return m_alias;
}

void SimCard::setAlias(const QString &alias)
{
    m_alias = alias;
}

SimBalance *SimCard::balance() const
{
    return m_balance;
}

void SimCard::setBalance(SimBalance *balance)
{
    m_balance = balance;
    emit balanceChanged();
}

QList<Usage*> SimCard::usageEntries() const
{
    return m_usageEntries;
}

void SimCard::setUsageEntries(const QList<Usage*> &usageEntries)
{
    m_usageEntries = usageEntries;
}

