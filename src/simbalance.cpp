#include "simbalance.h"
#include "quantity.h"
#include "simcard.h"

bool quantityDateComparer(Quantity* q1, Quantity* q2)
{
    return q1->startDate() < q2->startDate();
}

Quantity* SimBalance::getBundle(SimCard::TrafficType type, unsigned int bundleId) const
{
    QList<Quantity*> quantities;

    foreach (Quantity* q, m_bundles)
        if (q->typeId() == type)
            quantities << q;

    if (quantities.count() == 0)
        return NULL;

    qSort(quantities.begin(), quantities.end(), quantityDateComparer);

    return quantities[bundleId];
}

unsigned int SimBalance::getBundleCount(SimCard::TrafficType type) const
{
    unsigned int count = 0;

    foreach (Quantity* q, m_bundles)
        if (q->typeId() == type)
            count++;

    return count;
}
