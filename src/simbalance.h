#ifndef SIMBALANCE_H
#define SIMBALANCE_H

#include <QObject>
#include "quantity.h"
#include "simcard.h"

class SimBalance : public QObject
{
    Q_OBJECT
public:

    Q_PROPERTY(QQmlListProperty<Quantity> bundles READ bundles NOTIFY bundlesChanged)

    SimBalance(QObject *parent = NULL) : QObject(parent) {}
    ~SimBalance()
    {
    }

    QString pricePlan;
    QDateTime validity;
    bool expired;
    QString credits;
    QByteArray latestJson;

    QQmlListProperty<Quantity> bundles()
    {
        return QQmlListProperty<Quantity>(this, m_bundles);
    }

    void setBundles(QList<Quantity*> bundles)
    {
        foreach (Quantity *q, m_bundles)
        {
            q->deleteLater();
        }

        m_bundles = bundles;

        emit bundlesChanged();
    }

public slots:
    Q_INVOKABLE unsigned int smsMvBundleCount() const { return getBundleCount(SimCard::SMS_MV_TRAFFIC); }
    Q_INVOKABLE unsigned int voiceMvBundleCount() const { return getBundleCount(SimCard::VOICE_MV_TRAFFIC); }
    Q_INVOKABLE unsigned int smsBundleCount() const { return getBundleCount(SimCard::SMS_TRAFFIC); }
    Q_INVOKABLE unsigned int dataBundleCount() const { return getBundleCount(SimCard::DATA_TRAFFIC); }
    Q_INVOKABLE Quantity* smsMvBundle(unsigned int bundleId = 0) const { return getBundle(SimCard::SMS_MV_TRAFFIC, bundleId); }
    Q_INVOKABLE Quantity* voiceMvBundle(unsigned int bundleId = 0) const { return getBundle(SimCard::VOICE_MV_TRAFFIC, bundleId); }
    Q_INVOKABLE Quantity* smsBundle(unsigned int bundleId = 0) const { return getBundle(SimCard::SMS_TRAFFIC, bundleId); }
    Q_INVOKABLE Quantity* dataBundle(unsigned int bundleId = 0) const { return getBundle(SimCard::DATA_TRAFFIC, bundleId); }
    Q_INVOKABLE int daysLeft() const { return QDateTime::currentDateTime().daysTo(validity); }
    Q_INVOKABLE QString creditLeft() const { return credits; }

signals:
    void bundlesChanged();

private:
    QList<Quantity*> m_bundles;
    Quantity* getBundle(SimCard::TrafficType type, unsigned int bundleId) const;
    unsigned int getBundleCount(SimCard::TrafficType type) const;
};

#endif // SIMBALANCE_H
