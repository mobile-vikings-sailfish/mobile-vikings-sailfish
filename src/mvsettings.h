#ifndef MVSETTINGS_H
#define MVSETTINGS_H

#include <QSettings>

class SimCard;

class MVSettings : public QSettings
{
    Q_OBJECT
public:
    explicit MVSettings(QObject *parent = 0);
    ~MVSettings();

signals:

public slots:
    void setPassword(const QString& password);
    void setUserName(const QString& userName);
    void setSettingsVersion(const QString& version);
    void setSelectedSim(int selectedSim);
    void setSimCards(const QList<SimCard*>);

    QString userName() const;
    QString password() const;
    QString settingsVersion() const;
    int selectedSim() const;
    QList<SimCard*> simCards() const;
};

#endif // MVSETTINGS_H
