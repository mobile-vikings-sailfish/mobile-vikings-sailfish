#ifndef OXTMOBILEVIKINGS_H
#define OXTMOBILEVIKINGS_H

#include "o1.h"
#include <QSettings>

class OXMobileVikings: public O1 {
    Q_OBJECT

public:
    explicit OXMobileVikings(QSettings *settings, QObject *parent = 0);
    /// MV XAuth login parameters
    /// XAuth Username
    Q_PROPERTY(QString username READ username WRITE setUsername NOTIFY usernameChanged)
    QString username();
    void setUsername(const QString &username);

    /// XAuth Password
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    QString password();
    void setPassword(const QString &username);

public slots:
    /// Authenticate.
    Q_INVOKABLE virtual void link();

    Q_INVOKABLE QNetworkReply *sendRequest(QNetworkAccessManager* manager, QNetworkRequest request);

signals:
    void usernameChanged();
    void passwordChanged();

private:
    QList<O1RequestParameter> xAuthParams_;
    QString username_;
    QString password_;
};

#endif // OXTMOBILEVIKINGS_H
