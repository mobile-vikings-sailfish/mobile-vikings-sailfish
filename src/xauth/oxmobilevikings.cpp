#include <QDateTime>
#include <QDebug>
#include <QUrlQuery>
#include <QPair>

#include "oxmobilevikings.h"
#include "o2globals.h"
#include "o2settingsstore.h"
#include "o1requestor.h"

#define trace() if (1) qDebug()

#define SSUB(x)         #x
#define STRINGIZE(x)    SSUB(x)

#define REQUEST_TOKEN_URL "https://mobilevikings.be/api/2.0/oauth/request_token/"
#define AUTHORIZE_TOKEN_URL "https://mobilevikings.be/api/2.0/oauth/authorize/"
#define ACCESS_TOKEN_URL "https://mobilevikings.be/api/2.0/oauth/access_token/"

const char XAUTH_USERNAME[] = "x_auth_username";
const char XAUTH_PASSWORD[] = "x_auth_password";
const char XAUTH_MODE[] = "x_auth_mode";
const char XAUTH_MODE_VALUE[] = "client_auth";

OXMobileVikings::OXMobileVikings(QSettings *settings, QObject *parent) :
    O1(parent)
{
    this->setAuthorizeUrl(QUrl(AUTHORIZE_TOKEN_URL));
    this->setRequestTokenUrl(QUrl(REQUEST_TOKEN_URL));
    this->setAccessTokenUrl(QUrl(ACCESS_TOKEN_URL));

    O2SettingsStore *settingsStore = new O2SettingsStore(settings, STRINGIZE(APP_CRYPT_KEY), this);
    settingsStore->setGroupKey("mv");
    this->setStore(settingsStore);

    this->setClientId(STRINGIZE(MV_KEY));
    this->setClientSecret(STRINGIZE(MV_SECRET));
    // Compile error here ? Add the line
    //      "DEFINES += APP_CRYPT_KEY=your_crypt_key" "DEFINES += MV_KEY=your_mv_key" "DEFINES += MV_SECRET=your_mv_secret"
    // to the additionnal arguments of the qmake Build step.

    // The APP_CRYPT_KEY is the one that encrypts the secret key and token in the config file. (It won't work with the app from jolla harbour)
    // The MV_KEY and MV_SECRET are the Key and Secret provided by your Mobile Vikings account (https://mobilevikings.be/fr/account/edit/oauth-credentials/)
    // They are not distrubuted here because they are the identifier of the application
    // and could be used to fake it by another app and behave badly.
}

QString OXMobileVikings::username()
{
    return username_;
}

void OXMobileVikings::setUsername(const QString &username) {
    username_ = username;
    emit usernameChanged();
}

QString OXMobileVikings::password() {
    return password_;
}

void OXMobileVikings::setPassword(const QString &password) {
    password_ = password;
    emit passwordChanged();
}

void OXMobileVikings::link() {
    if (linked()) {
        trace() << "Linked already";
        return;
    }

    if (username_.isEmpty() || password_.isEmpty()) {
        qWarning() << "Error: XAuth parameters not set. Aborting!";
        return;
    }

    // prepare XAuth parameters
    xAuthParams_.append(O1RequestParameter(QByteArray(XAUTH_USERNAME), username_.toLatin1()));
    xAuthParams_.append(O1RequestParameter(QByteArray(XAUTH_PASSWORD), password_.toLatin1()));
    xAuthParams_.append(O1RequestParameter(QByteArray(XAUTH_MODE), QByteArray(XAUTH_MODE_VALUE)));

    QList<O1RequestParameter> oauthParams;
    oauthParams.append(O1RequestParameter(O2_OAUTH_NONCE, nonce()));
    oauthParams.append(O1RequestParameter(O2_OAUTH_TIMESTAMP, QString::number(QDateTime::currentDateTimeUtc().toTime_t()).toLatin1()));
    oauthParams.append(O1RequestParameter(O2_OAUTH_CONSUMER_KEY, clientId().toLatin1()));

    oauthParams.append(O1RequestParameter(O2_OAUTH_SIGNATURE_METHOD, O2_SIGNATURE_TYPE_HMAC_SHA1));
    oauthParams.append(O1RequestParameter(O2_OAUTH_VERSION, "1.0"));

    oauthParams.append(O1RequestParameter(O2_OAUTH_TOKEN, QByteArray("")));
    oauthParams.append(O1RequestParameter(O2_OAUTH_VERFIER, QByteArray("")));

    QByteArray signature = sign(oauthParams, xAuthParams_, accessTokenUrl(), QNetworkAccessManager::PostOperation, clientSecret(), "");
    oauthParams.append(O1RequestParameter(O2_OAUTH_SIGNATURE, signature));

    // Post request
    QNetworkRequest request(accessTokenUrl());
    request.setHeader(QNetworkRequest::ContentTypeHeader, O2_MIME_TYPE_XFORM);

    QNetworkReply *reply = manager_->post(request, buildLinkAuthorizationHeader(oauthParams) + "&" + createQueryParams(xAuthParams_));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(onTokenExchangeError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), this, SLOT(onTokenExchangeFinished()));
}

QNetworkReply *OXMobileVikings::sendRequest(QNetworkAccessManager* manager, QNetworkRequest request)
{
    O1Requestor* requestor = new O1Requestor(manager, this, this);
    QList<O1RequestParameter> reqParams = QList<O1RequestParameter>();
    QUrlQuery query(request.url());

    for (int i = 0; i < query.queryItems().count(); i++)
    {
        QPair<QString, QString> item = query.queryItems().at(i);
        reqParams << O1RequestParameter(item.first.toLatin1(), item.second.toLatin1());
    }

    request.setHeader(QNetworkRequest::ContentTypeHeader, O2_MIME_TYPE_XFORM);

    return requestor->get(request, reqParams);
}
