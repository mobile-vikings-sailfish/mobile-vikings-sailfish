#include "usage.h"

double Usage::price() const
{
    return m_price;
}

void Usage::setPrice(double price)
{
    m_price = price;
}
double Usage::balance() const
{
    return m_balance;
}

void Usage::setBalance(double balance)
{
    m_balance = balance;
}
SimCard::TrafficType Usage::trafficType() const
{
    return m_trafficType;
}

void Usage::setTrafficType(const SimCard::TrafficType &trafficType)
{
    m_trafficType = trafficType;
}
QDateTime Usage::start() const
{
    return m_start;
}

void Usage::setStart(const QDateTime &start)
{
    m_start = start;
}
QDateTime Usage::end() const
{
    return m_end;
}

void Usage::setEnd(const QDateTime &end)
{
    m_end = end;
}
int Usage::durationCall() const
{
    return m_durationCall;
}

void Usage::setDurationCall(int durationCall)
{
    m_durationCall = durationCall;
}
int Usage::durationConnection() const
{
    return m_durationConnection;
}

void Usage::setDurationConnection(int durationConnection)
{
    m_durationConnection = durationConnection;
}
unsigned long long Usage::timestamp() const
{
    return m_timestamp;
}

void Usage::setTimestamp(unsigned long long timestamp)
{
    m_timestamp = timestamp;
}
QString Usage::durationPretty() const
{
    return m_durationPretty;
}

void Usage::setDurationPretty(const QString &durationPretty)
{
    m_durationPretty = durationPretty;
}
QString Usage::to() const
{
    return m_to;
}

void Usage::setTo(const QString &value)
{
    m_to = value;
}

Usage::UsageDirection Usage::direction() const
{
    return m_direction;
}

void Usage::setDirection(const UsageDirection &direction)
{
    m_direction = direction;
}


