#ifndef QUANTITY_H
#define QUANTITY_H

#include <QObject>
#include <QDebug>
#include <QDateTime>

#include "simcard.h"

class Quantity : public QObject
{
    Q_OBJECT
public:

    Q_PROPERTY(double amount READ amount WRITE setAmount NOTIFY amountChanged)
    Q_PROPERTY(double assigned READ assigned WRITE setAssigned NOTIFY assignedChanged)
    Q_PROPERTY(double progress READ progress NOTIFY progressChanged)

    Quantity() {}
    Quantity(const QString& type, SimCard::TrafficType typeId, double amount, const QDateTime& start = QDateTime::currentDateTime(), QObject *parent = NULL) : QObject(parent)
    {
        this->m_type = type;
        this->m_typeId = typeId;
        this->m_amount = amount;
        m_dateTime = start;
        //qDebug() << QString("Quantity created (0x%1)").arg((int)this);
    }

    ~Quantity()
    {
        //qDebug() << QString("Quantity destroyed (0x%1)").arg((int)this);
    }

    QString type() const;
    void setType(const QString &type);

    SimCard::TrafficType typeId() const;
    void setTypeId(const SimCard::TrafficType &typeId);

    double amount() const;
    void setAmount(double amount);

    double assigned() const;
    void setAssigned(double assigned);

    double progress() const;
    void setProgress(double progress);

    QDateTime startDate() const { return m_dateTime; }

//    void deleteLater()
//    {
//        qDebug() << QString("Deleting later (%1)").arg((long int)this);
//        QObject::deleteLater();
//    }

signals:
    void typeChanged();
    void typeIdChanged();
    void amountChanged();
    void assignedChanged();
    void progressChanged();

private:
    QString m_type;
    SimCard::TrafficType m_typeId;
    double m_amount;
    double m_assigned;

    QDateTime m_dateTime;
};

#endif // QUANTITY_H
