#ifndef USAGE_H
#define USAGE_H

#include <QObject>

#include "simcard.h"

class Usage : public QObject
{
    Q_OBJECT
    Q_ENUMS(UsageDirection)
    Q_PROPERTY(double price READ price CONSTANT)
    Q_PROPERTY(double balance READ balance CONSTANT)
    Q_PROPERTY(SimCard::TrafficType trafficType READ trafficType CONSTANT)
    Q_PROPERTY(QDateTime start READ start CONSTANT)
    Q_PROPERTY(QDateTime end READ end CONSTANT)
    Q_PROPERTY(int durationCall READ durationCall CONSTANT)
    Q_PROPERTY(int durationConnection READ durationConnection CONSTANT)
    Q_PROPERTY(unsigned long long timestamp READ timestamp CONSTANT)
    Q_PROPERTY(QString durationPretty READ durationPretty CONSTANT)
    Q_PROPERTY(QString to READ to CONSTANT)
    Q_PROPERTY(UsageDirection direction READ direction CONSTANT)


public:
    enum UsageDirection { DIRECTION_TO, DIRECTION_FROM };

    Usage(QObject* parent = NULL) : QObject(parent) {}

    double price() const;
    Q_INVOKABLE void setPrice(double price);

    double balance() const;
    Q_INVOKABLE void setBalance(double balance);

    SimCard::TrafficType trafficType() const;
    Q_INVOKABLE void setTrafficType(const SimCard::TrafficType &trafficType);

    QDateTime start() const;
    Q_INVOKABLE void setStart(const QDateTime &start);

    QDateTime end() const;
    Q_INVOKABLE void setEnd(const QDateTime &end);

    int durationCall() const;
    Q_INVOKABLE void setDurationCall(int durationCall);

    int durationConnection() const;
    Q_INVOKABLE void setDurationConnection(int durationConnection);

    unsigned long long timestamp() const;
    Q_INVOKABLE void setTimestamp(unsigned long long timestamp);

    QString durationPretty() const;
    Q_INVOKABLE void setDurationPretty(const QString &durationPretty);

    QString to() const;
    Q_INVOKABLE void setTo(const QString &value);

    UsageDirection direction() const;
    void setDirection(const UsageDirection &direction);

private:
    QDateTime m_start;
    QDateTime m_end;

    SimCard::TrafficType m_trafficType;

    double m_balance;
    double m_price;

    int m_durationCall;
    int m_durationConnection;
    long long unsigned int m_timestamp;

    QString m_durationPretty;
    QString m_to;
    UsageDirection m_direction;
};



#endif // USAGE_H
