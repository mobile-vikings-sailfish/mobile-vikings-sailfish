#include "mobilevikingconnection.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <QTimer>

#include <QFile>
#include <QSettings>
#include <QDir>
#include <QStandardPaths>
#include <QCoreApplication>


/**
 * MobileVikingConnection members
 */


MobileVikingConnection::MobileVikingConnection(AuthenticationType type, QObject *parent) :
    QObject(parent), m_type(type)
{
    m_updating = false;
    m_currentDate = QDate::currentDate();

    m_oxmv = new OXMobileVikings(&m_settings, this);

    QObject::connect(m_oxmv, SIGNAL(linkingSucceeded()), this, SIGNAL(connected()));
    QObject::connect(m_oxmv, SIGNAL(linkingSucceeded()), this, SIGNAL(linkedChanged()));
    QObject::connect(m_oxmv, SIGNAL(linkingFailed()), this, SIGNAL(loginError()));
    QObject::connect(m_oxmv, SIGNAL(linkingFailed()), this, SIGNAL(linkedChanged()));

    QObject::connect(&m_networkManager, SIGNAL(finished(QNetworkReply*)), SLOT(receivedReply(QNetworkReply*)));
    QObject::connect(&m_timeout, SIGNAL(timeout()), SLOT(timeoutSlot()));

    connect();

    setSelectedSim(m_settings.selectedSim());

    if (m_settings.settingsVersion() == "0.0")
    {
        m_settings.setSimCards(m_sims);
        m_settings.setSettingsVersion("1.0");
        if (selectedSim() != -1)
            QTimer::singleShot(1, this, SLOT(getSimCards()));
    }
    else
    {
        m_sims = m_settings.simCards();
        if (selectedSim() != -1)
            QTimer::singleShot(1, this, SLOT(update()));
    }
}

MobileVikingConnection::~MobileVikingConnection()
{
    m_settings.setSimCards(m_sims);
    qDeleteAll(m_sims);

    delete m_oxmv;
}

void MobileVikingConnection::timeoutSlot()
{
    setUpdating(false);
    setErrorInfo("Connection timeout");
    m_networkReply->abort();
    emit hostError();
}

void MobileVikingConnection::connect()
{
    switch (m_type)
    {
    case MobileVikingConnection::BASIC:
    {
        QByteArray ba;
        ba.append(m_userName + ":" + m_password);

        m_authorizationString = ba.toBase64();

        emit connected();
        break;
    }
    case MobileVikingConnection::OAUTH:
    {
        if (m_oxmv->linked())
        {
            emit connected();
            return;
        }

        m_oxmv->setUsername(m_userName);
        m_oxmv->setPassword(m_password);

        m_oxmv->link();

        break;
    }
    }
}

void MobileVikingConnection::getSimCards()
{
    getSimCards(false);
}

QString MobileVikingConnection::errorInfo() const
{
    return m_errorInfo;
}

void MobileVikingConnection::setErrorInfo(const QString &errorInfo)
{
    m_errorInfo = errorInfo;
    emit errorInfoChanged();
}


bool MobileVikingConnection::updating() const
{
    return m_updating;
}

void MobileVikingConnection::setUpdating(bool updating)
{
    m_updating = updating;
    emit updatingChanged();
}

int MobileVikingConnection::selectedSim() const
{
    return m_selectedSim;
}

void MobileVikingConnection::setSelectedSim(int selectedSim)
{
    m_selectedSim = selectedSim;
    m_settings.setSelectedSim(selectedSim);
    emit selectedSimChanged();
}


void MobileVikingConnection::getSimCards(bool alias)
{
    QHash<QString, QString> hash;

    if (alias)
        hash.insert("alias", "1");

    m_sendGetRequest(MSISDN_REQUEST, hash);
}

void MobileVikingConnection::getPricePlanDetails(SimCard* simCard)
{
    QHash<QString, QString> hash;

    hash.insert("msisdn", simCard->msisdn());

    m_sendGetRequest(PRICE_PLAN_REQUEST, hash);
}

void MobileVikingConnection::getSimBalance(SimCard* simCard)
{
    QHash<QString, QString> hash;

    hash.insert("msisdn", simCard->msisdn());

    m_sendGetRequest(SIM_BALANCE_REQUEST, hash);
}

void MobileVikingConnection::getTopUpHistory(SimCard* sim, const QDateTime& from, const QDateTime& until, int pageSize, int pageNumber)
{
    QHash<QString, QString> hash;

    hash.insert("msisdn", sim->msisdn());
    hash.insert("from_date", from.toString("yyyy-MM-ddThh:mm:ss"));
    hash.insert("until_date", until.toString("yyyy-MM-ddThh:mm:ss"));
    hash.insert("page_size", QString::number(pageSize));
    hash.insert("page", QString::number(pageNumber));

    m_sendGetRequest(TOPUP_HISTORY_REQUEST, hash);
}

void MobileVikingConnection::getUsageHistory(SimCard* sim, const QDateTime& from, const QDateTime& until, int pageSize, int pageNumber, bool addPricePlan)
{
    QHash<QString, QString> hash;

    hash.insert("msisdn", sim->msisdn());
    hash.insert("from_date", from.toString("yyyy-MM-ddThh:mm:ss"));
    hash.insert("until_date", until.toString("yyyy-MM-ddThh:mm:ss"));
    hash.insert("page_size", QString::number(pageSize));
    hash.insert("page", QString::number(pageNumber));
    hash.insert("add_price_plan", addPricePlan ? "1" : "0");

    m_sendGetRequest(USAGE_REQUEST, hash);
}

void MobileVikingConnection::update()
{
    if (selectedSim() >= 0 && selectedSim() < m_sims.count())
        getSimBalance(m_sims[selectedSim()]);
}

bool MobileVikingConnection::m_sendGetRequest(RequestType reqType, const QHash<QString, QString> &options)
{
    if (updating() || !m_oxmv->linked())
        return false;

    // Set a 30 seconds timeout.
    m_timeout.start(30000);

    setUpdating(true);

    QString url = "https://mobilevikings.be/api/2.0/";

    switch(m_type)
    {
    case BASIC:
        url += "basic/";
        break;
    case OAUTH:
        url += "oauth/";
        break;
    }

    switch(reqType)
    {
    case MSISDN_REQUEST:
        url += "msisdn_list.";
        break;
    case PRICE_PLAN_REQUEST:
        url += "msisdn_list.";
        break;
    case SIM_BALANCE_REQUEST:
        url += "sim_balance.";
        break;
    case TOPUP_HISTORY_REQUEST:
        url += "msisdn_list.";
        break;
    case USAGE_REQUEST:
        url += "usage.";
        break;
    case SIMCARD_INFO_REQUEST:
        url += "sim_info.";
        break;
    }

    // Only json is used.
    url += "json";

    if (options.count())
        url += "?";

    foreach (QString key, options.keys())
    {
        url += key + "=" + options.value(key) + "&";
    }

    if (options.count())
        url.remove(url.length() - 1, 1);

    QNetworkRequest r;
    r.setUrl(QUrl::fromUserInput(url));

    qDebug() << "url:" << r.url();

    if (m_type == BASIC)
    {
        QString headerData = "Basic " + m_authorizationString;
        r.setRawHeader("Authorization", headerData.toLocal8Bit());

        qDebug() << "Sending request :" << r.url();

        m_networkReply = m_networkManager.get(r);
    }
    else if (m_type == OAUTH)
    {
        m_networkReply = m_oxmv->sendRequest(&m_networkManager, r);
    }

    return true;
}

void MobileVikingConnection::receivedReply(QNetworkReply* reply)
{
    qDebug() << "Got Reply ! " << reply->url().url();
    m_timeout.stop();
    if (reply->error() != QNetworkReply::NoError)
    {
        qDebug() << "reply error " << reply->errorString();
        //qDebug() << m_userName << m_password;
        setErrorInfo(reply->errorString());

        setUpdating(false);

        if (reply->error() == QNetworkReply::AuthenticationRequiredError)
            emit loginError();
        else
            emit hostError();

        m_networkReply->deleteLater();
        return;
    }

    if (reply->url().url().contains("msisdn_list.json"))
    {
        foreach (SimCard *card, m_sims) {
            card->deleteLater();
        }

        m_sims.clear();

        m_sims = SimCard::fromData(reply->readAll());

        emit simCardsChanged();

        if (m_sims.count() == 0)
        {
            setUpdating(false);

            m_networkReply->deleteLater();
            return;
        }

        m_simsLeft = m_sims.count();

        foreach (SimCard* sim, m_sims)
        {
            QHash<QString, QString> hash;

            hash.insert("msisdn", sim->msisdn());

            //FIXME:Only send one request at a time
            m_updating = false;
            m_sendGetRequest(SIMCARD_INFO_REQUEST, hash);
            m_updating = true;
        }
    }
    else if (reply->url().url().contains("sim_info"))
    {
        QUrlQuery urlQuery(reply->url());

        foreach (SimCard *sim, m_sims)
        {
            if (sim->msisdn() == urlQuery.queryItemValue("msisdn"))
            {
                sim->setInfoFromData(reply->readAll());
                m_simsLeft--;
                break;
            }
        }

        if (m_simsLeft == 0)
        {
            foreach (SimCard *card, m_sims)
            {
                qDebug() << card->alias() + " (" + card->number() + ")";
            }

            setUpdating(false);

            if (m_sims.count() > 0)
                setSelectedSim(m_selectedSim >= m_sims.count() || m_selectedSim == -1 ? 0 : m_selectedSim);
            else
                setSelectedSim(-1);

            emit simCardsReady();
        }
    }
    else if (reply->url().url().contains("sim_balance"))
    {
        setUpdating(false);
        QUrlQuery urlQuery(reply->url());

        foreach (SimCard *sim, m_sims)
        {
            if (sim->msisdn() == urlQuery.queryItemValue("msisdn"))
            {
                sim->setBalanceFromData(reply->readAll());
                break;
            }
        }
    }
    else if (reply->url().url().contains("usage"))
    {
        qDebug() << "Usage reply !";
        SimCard *card = NULL;
        QUrlQuery urlQuery(reply->url());
        foreach (SimCard *sim, m_sims)
        {
            if (sim->msisdn() == urlQuery.queryItemValue("msisdn"))
            {
                card = sim;
            }
        }

        if (!m_gettingUsage)
        {
            m_gettingUsage = true;
            m_page = 2;
            if (25 == card->setUsageFromData(reply->readAll()))
            {
                m_updating = false;
                getUsageHistory(card, QDateTime(m_currentDate, QTime(0,0)), QDateTime(m_currentDate, QTime(23,59,59)), 25, m_page++);
                m_updating = true;
            }
            else
            {
                setUpdating(false);
                m_gettingUsage = false;
            }
        }
        else if (25 == card->addUsageFromData(reply->readAll()))
        {
            m_updating = false;
            getUsageHistory(card, QDateTime(m_currentDate, QTime(0,0)), QDateTime(m_currentDate, QTime(23,59,59)), 25, m_page++);
            m_updating = true;
        }
        else
        {
            setUpdating(false);
            m_gettingUsage = false;
        }
    }
}

void MobileVikingConnection::unlink()
{
    m_oxmv->unlink();

    foreach (SimCard *card, m_sims) {
        card->deleteLater();
    }

    m_sims.clear();

    setSelectedSim(-1);

    emit simCardsChanged();
    emit linkedChanged();
}

void MobileVikingConnection::updateUsage(const QDate &date)
{
    if (!date.isNull())
        m_currentDate = date;

    m_gettingUsage = false;

    getUsageHistory(m_sims[selectedSim()], QDateTime(m_currentDate, QTime(0,0)), QDateTime(m_currentDate, QTime(23,59,59)));

}
