#ifndef SIMCARD_H
#define SIMCARD_H

#include <QObject>

#include <QDateTime>
#include <QNetworkAccessManager>
#include <QQmlListProperty>

//#include "quantity.h"
//#include "simbalance.h"
//#include "usage.h"

class Usage;
class SimBalance;

/*class PricePlan
{
public:
    PricePlan();
    QList<Quantity*> prices;
    QList<Quantity*> bundle;
    QString name;
    double topUpAmount;
};

class TopUp
{
public:
    TopUp();

    QString status;
    double amount;
    double amountExVat;
    QString method;
    QDateTime executionDate;
    QDateTime paymentDate;
};*/

class SimCard : public QObject
{
    Q_OBJECT
    Q_ENUMS(TrafficType)

public:

    enum TrafficType {
        //The type and type_id represent a traffic type:

        VOICE_TRAFFIC = 1,
        DATA_TRAFFIC = 2,
        SMS_TRAFFIC = 5,
        MMS_TRAFFIC = 7,
        VOICE_MV_TRAFFIC = 11,
        SMS_MV_TRAFFIC = 15,
        UNKNOWN_TRAFFIC = 0xff
    };

    Q_PROPERTY(QString msisdn READ msisdn NOTIFY msisdnChanged)
    Q_PROPERTY(SimBalance* balance READ balance WRITE setBalance NOTIFY balanceChanged)
    Q_PROPERTY(QQmlListProperty<Usage> qmlUsage READ qmlUsage NOTIFY usageChanged)

    SimCard(const QString& number) : m_msisdn(number), m_balance(0) {}
    SimCard() {}

    ~SimCard();

    static QList<SimCard*> fromData(const QByteArray& data);
    void setInfoFromData(const QByteArray& data);
    void setBalanceFromData(const QByteArray& data);

    /**
     * @brief setUsageFromData
     * @param data
     * @return the number of usage entries added to the usage info.
     */
    unsigned int setUsageFromData(const QByteArray& data);
    unsigned int addUsageFromData(const QByteArray& data);

    QString number() const {return m_msisdn;}
    QString msisdn() const {return m_msisdn;}

    QString imsi() const;
    void setImsi(const QString &imsi);

    QString puk2() const;
    void setPuk2(const QString &puk2);

    QString puk1() const;
    void setPuk1(const QString &puk1);

    QString pin2() const;
    void setPin2(const QString &pin2);

    QString pin1() const;
    void setPin1(const QString &pin1);

    QString cardNumber() const;
    void setCardNumber(const QString &cardNumber);

    QString alias() const;
    void setAlias(const QString &alias);

    SimBalance *balance() const;
    void setBalance(SimBalance *balance);

    QList<Usage*> usageEntries() const;
    void setUsageEntries(const QList<Usage*> &usageEntries);

    QQmlListProperty<Usage> qmlUsage() {
        return QQmlListProperty<Usage>(this, m_usageEntries);
    }

signals:
    void msisdnChanged();
    void balanceChanged();
    void usageChanged();

private:
    QString m_msisdn;
    QString m_cardNumber;
    QString m_pin1;
    QString m_pin2;
    QString m_puk1;
    QString m_puk2;
    QString m_imsi;
    QString m_alias;
    SimBalance *m_balance;
    QList<Usage*> m_usageEntries;

    TrafficType typeStringtoTypeId(const QString& typeString) const;
};

#endif // SIMCARD_H
