<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>ConnectingPage</name>
    <message>
        <source>Retrieving SIMs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check API status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Show Page 2</source>
        <translation type="obsolete">Zur Seite 2</translation>
    </message>
    <message>
        <source>UI Template</source>
        <translation type="obsolete">UI-Vorlage</translation>
    </message>
    <message>
        <source>Hello Sailors</source>
        <translation type="obsolete">Hallo Matrosen</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Validity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Voice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SecondPage</name>
    <message>
        <source>Nested Page</source>
        <translation type="obsolete">Unterseite</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Update Sims</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linked to Mobile Vikings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Press and hold to unlink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unlink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unlinking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid username or password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sim Card : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UsagePage</name>
    <message>
        <source>Set date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>From:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Free</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No usage entry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
