import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    property alias spinning: busyAnim.running
    property alias image: busyIndicatorCenterImg.source

    anchors.fill: parent

    Image {
        id: busyIndicatorCenterImg

        anchors.centerIn: parent
        width: busyIndicator.width / 2
        height: width
        visible: source != null
    }

    ProgressCircle {
        id: busyIndicator

        anchors.centerIn: parent
        width: parent.width / 2
        height: width

        value: 0.77

        NumberAnimation {
            id: busyAnim

            target: busyIndicator
            properties: "rotation"
            from: 0
            to: 359
            loops: Animation.Infinite
            duration: 1000
        }
    }
}
