/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../pages"

CoverBackground {
    width: Cover.size === Cover.Large ? Theme.coverSizeLarge.width : Theme.coverSizeSmall.width
    height: Cover.size === Cover.Large ? Theme.coverSizeLarge.height : Theme.coverSizeSmall.height

    Connections {
        target: mv.selectedSim === -1 ? null : mv.simCards[mv.selectedSim]
        onBalanceChanged: {
            var balance = mv.simCards[mv.selectedSim].balance

            if (balance !== null)
            {

                if (balance.smsMvBundle() !== null)
                {
                    smsMVProgress.value = balance.smsMvBundle().progress
                    smsMVProgress.prettyValue = balance.smsMvBundle().amount
                }

                smsMVProgress.visible = balance.smsMvBundle() !== null

                if (balance.voiceMvBundle() !== null)
                {
                    voiceProgress.value = balance.voiceMvBundle().progress
                    voiceProgress.prettyValue = Math.floor(balance.voiceMvBundle().amount/60) + " " + qsTr("Min")
                }

                voiceProgress.visible = balance.voiceMvBundle() !== null

                if (balance.smsBundle() !== null)
                {
                    smsProgress.value = balance.smsBundle().progress
                    smsProgress.prettyValue = balance.smsBundle().amount
                }

                smsProgress.visible = balance.smsBundle() !== null

                if (balance.dataBundle() !== null)
                {
                    dataProgress.value = balance.dataBundle().progress
                    dataProgress.prettyValue = Math.floor(balance.dataBundle().amount/1024/1024) + " MB"
                }

                dataProgress.visible = balance.dataBundle() !== null

                creditLeft.prettyValue = balance.creditLeft() + " €";
                creditLeft.visible = !voiceProgress.visible
            }
            else
            {
                smsMVProgress.value = 0
                voiceProgress.value = 0
                smsProgress.value = 0
                dataProgress.value = 0
                timeLeft.value = 0

                smsMVProgress.prettyValue = 0
                voiceProgress.prettyValue = 0 + " " + qsTr("Min")
                smsProgress.prettyValue = 0
                dataProgress.prettyValue = 0 + " MB"
                //timeLeft.prettyValue = ""
                //creditLeft.prettyValue = 0 + " €";
            }
        }
    }

    Connections {
        target: mv
        onSelectedSimChanged:
        {
            if (mv.selectedSim === -1)
            {
                smsMVProgress.value = 0
                voiceProgress.value = 0
                smsProgress.value = 0
                dataProgress.value = 0
                //timeLeft.value = 0

                smsMVProgress.prettyValue = 0
                voiceProgress.prettyValue = 0 + " " + qsTr("Min")
                smsProgress.prettyValue = 0
                dataProgress.prettyValue = 0 + " MB"
                //timeLeft.prettyValue = ""
                //creditLeft.prettyValue = 0 + " €";
            }
        }
    }

    CoverAmountState {
        id: smsProgress
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        value: 0
        prettyValue: ""
        width: parent.width
        height: Cover.size === Cover.Large ? Theme.coverSizeLarge.height/4 : Theme.coverSizeSmall.height/4
        icon:  "image://theme/icon-m-sms"
    }

    CoverAmountState {
        id: dataProgress
        anchors.top: smsProgress.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        value: 0
        prettyValue: ""
        width: parent.width
        height: Cover.size === Cover.Large ? Theme.coverSizeLarge.height/4 : Theme.coverSizeSmall.height/4
        icon:  "image://theme/icon-m-cloud-download"
    }

    CoverAmountState {
        id: voiceProgress
        anchors.top: dataProgress.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        value: 0
        prettyValue: ""
        width: parent.width
        height: Cover.size === Cover.Large ? Theme.coverSizeLarge.height/4 : Theme.coverSizeSmall.height/4
        icon: "image://theme/icon-l-answer"
        isViking: true
    }

    CoverAmountState {
        id: smsMVProgress
        anchors.top: voiceProgress.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        value: 0
        prettyValue: ""
        width: parent.width
        height: Cover.size === Cover.Large ? Theme.coverSizeLarge.height/4 : Theme.coverSizeSmall.height/4
        icon:  "image://theme/icon-m-sms"
        isViking: true
    }

    Rectangle {
        property string prettyValue: ""

        id:creditLeft
        radius: width/2
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: parent.width/6
        width: parent.width/1.3
        height: width
        color: Theme.rgba(Theme.highlightColor, 0.4)
        visible: false
        enabled: visible

        Text {
            id: textLabel1
            text: qsTr("Credit")
            font.pointSize: Theme.fontSizeSmall
            color: Theme.primaryColor
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: separator.top
        }

        Rectangle {
            id: separator
            width: parent.width/2
            height: 1
            color: Theme.highlightColor
            anchors.centerIn: parent
        }

        Text {
            id: textLabel2
            text: parent.prettyValue
            font.pointSize: Theme.fontSizeSmall
            color: Theme.primaryColor
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: separator.bottom
        }
    }

    Rectangle {
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, 0.5)

        enabled: mv.updating
        visible: mv.updating
        opacity: mv.updating ? 1.0 : 0.0

        // BusyIndicator is paused when the application is in the background.
        // Therefore, it can't be used on cover page. This is why we created CoverBusyIndicator!
        CoverBusyIndicator {
            anchors.centerIn: parent
            spinning: mv.updating
            image: "image://theme/icon-cover-refresh"
        }

        Behavior on opacity {
            FadeAnimation {}
        }
    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-refresh"
            onTriggered:
            {
                if (!mv.updating)
                    mv.update()
            }
        }
    }
}


