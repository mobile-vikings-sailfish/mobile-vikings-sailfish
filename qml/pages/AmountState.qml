import QtQuick 2.0
import Sailfish.Silica 1.0

/*
Rectangle {
    //progressColor: Theme.highlightColor

    property string name
    property string prettyValue
    property bool isViking: false
    property double value: 0

    color: "green"
}*/

ProgressCircleBase {
    progressColor: Theme.highlightColor

    property string name
    property string prettyValue
    property bool isViking: false

    states: [
        State {
            name: "noPretty"
            AnchorChanges {
                target: textLabel1

                anchors.bottom: undefined
                anchors.verticalCenter: parent.verticalCenter
            }

            when: prettyValue === ""
        },
        State {
            name: "pretty"
            AnchorChanges {
                target: textLabel1

                anchors.verticalCenter: undefined
                anchors.bottom: separator.top
            }

            when: prettyValue !== ""
        }
    ]

    Behavior on value {
        NumberAnimation { easing.type: Easing.OutCubic; duration: 2000 }
    }

    Image {
        source: "qrc:///img/helmet.png"
        anchors.fill: parent
        anchors.margins: 20
        opacity: 0.4
        visible: isViking
    }

    Rectangle {
        id: separator
        width: parent.width/2
        height: 1
        color: Theme.highlightColor
        anchors.centerIn: parent
        visible: prettyValue !== ""
    }

    Text {
        id: textLabel1
        text: parent.name
        font.pointSize: Theme.fontSizeMedium
        color: Theme.primaryColor
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: textLabel2
        text: parent.prettyValue
        font.pointSize: Theme.fontSizeMedium
        color: Theme.primaryColor
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: separator.bottom
        visible: prettyValue !== ""
    }
}

