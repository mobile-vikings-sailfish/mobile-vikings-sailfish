/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.mobile.vikings.mv 1.0

//icon-m-cloud-download

Page {
    id: usagePage
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaListView {

        id:topupList
        anchors.fill: parent

        spacing: 5

        width: parent.width
        height: parent.height

        contentHeight: height

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            busy: mv.updating
            MenuItem {
                text: qsTr("Set date")
                onClicked: pageStack.push(Qt.resolvedUrl("DateChoosingDialog.qml"))
                enabled: !mv.updating && mv.linked
            }
            MenuItem {
                text: qsTr("Update")
                onClicked: mv.updateUsage()
                enabled: !mv.updating && mv.linked
            }
        }

        header: PageHeader {
            title: "Usage"
            id: header
        }

        Connections {
            target: mv
            onSelectedSimChanged:
            {
                topupList.model = mv.simCards[mv.selectedSim].qmlUsage
            }
        }

        function getPicture(modelData) {
            switch(modelData.trafficType)
            {
            case SimCard.DATA_TRAFFIC:
                return "image://theme/icon-m-cloud-download"
            case SimCard.SMS_MV_TRAFFIC:
            case SimCard.SMS_TRAFFIC:
                return "image://theme/icon-m-sms"
            case SimCard.VOICE_MV_TRAFFIC:
            case SimCard.VOICE_TRAFFIC:
                return "image://theme/icon-l-answer"
            }

            return "image://theme/icon-m-unknown"
        }

        model: mv.simCards[mv.selectedSim].qmlUsage
        delegate:
            ListItem {
            contentHeight: Theme.itemSizeMedium
            width: parent.width
            Image {
                id: image
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                anchors.verticalCenter: parent.verticalCenter

                source: topupList.getPicture(modelData)
                width: 64
                height: width
            }

            Label {
                text: (modelData.durationPretty === "n/a" ? "SMS" : modelData.durationPretty)

                color: Theme.primaryColor
                anchors.left: image.right
                anchors.leftMargin: Theme.paddingLarge
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: -5
            }

            Label {
                id:toText
                text: (modelData.direction === Usage.DIRECTION_TO ? qsTr("To:") : qsTr("From:")) + " " + modelData.to
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 5
                anchors.left: image.right
                anchors.leftMargin: Theme.paddingLarge
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                visible: modelData.trafficType !== SimCard.DATA_TRAFFIC
            }

            Image {
                anchors.left: toText.right
                anchors.leftMargin: Theme.paddingSmall
                source: "qrc:///img/helmet.png"
                height: toText.height
                width: height
                anchors.verticalCenter: toText.verticalCenter
                visible: modelData.trafficType === SimCard.SMS_MV_TRAFFIC || modelData.trafficType === SimCard.VOICE_MV_TRAFFIC
            }

            Label {
                text: (modelData.price === 0.0 ? qsTr("Free") : modelData.price + " €")
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: 10
                color: Theme.primaryColor
            }

            Label {
                text: Qt.formatDateTime(modelData.start, "dd/MM/yyyy, hh:mm")
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingSmall
                anchors.top: parent.top
                anchors.topMargin: 5
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
            }
        }


        VerticalScrollDecorator {}
    }

    Text {
        anchors.centerIn: parent
        visible: topupList.count === 0 && !mv.updating
        enabled: visible

        text: qsTr("No usage entry")
        color: Theme.secondaryColor
        font.pixelSize: Theme.fontSizeExtraLarge
    }
}


