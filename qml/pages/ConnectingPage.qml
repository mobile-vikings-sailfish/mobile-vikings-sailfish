import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id:connectingDialog
    allowedOrientations: Orientation.All

    canAccept: false

    onRejected: {
        console.log("Canceling connection...")
        //mv.cancelConnection()
    }

    Connections {
        target: mv

        onSimCardsReady: {
            connectingDialog.close()
            mv.update()
        }

        onConnected: {
            mv.getSimCards(true)
            textInfo.text = qsTr("Retrieving SIMs")
        }

        onHostError: {
            textInfo.text = qsTr("Check API status")
        }

        onLoginError: {
            connectingDialog.close()
        }
    }

    DialogHeader {
        title: qsTr("Connecting")
        id: header
    }

    BusyIndicator {
        id: indicator
        anchors.centerIn: parent
        running: true
        size: BusyIndicatorSize.Large
        enabled: visible
        visible: mv.updating || !mv.linked
    }

    Text {
        id: textInfo
        text: qsTr("Connecting...")
        color: Theme.primaryColor
        anchors.top: indicator.bottom
        anchors.topMargin: 15
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
