/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0


Dialog {
    id: page
    allowedOrientations: Orientation.All

    canAccept: !mv.updating

    Connections {
        target: mv
        onHostError: {

        }

        onLoginError: {
            userNameText.color = "red"
            passwordText.color = "red"
            wrongPasswordItem.visible = true
        }

        onConnected: {
            userNameText.color = Theme.primaryColor
            passwordText.color = Theme.primaryColor
            wrongPasswordItem.visible = false
        }

        onSelectedSimChanged: {
            simList.currentIndex = mv.selectedSim + 1
        }
    }

    ListView {
        id: listView
        anchors.fill: parent
        DialogHeader {
            id: header
            title: qsTr("Update Sims")
        }

        ListItem {
            id: linkedItem
            anchors.top: header.bottom
            contentHeight: Theme.itemSizeMedium
            width: parent.width
            visible: mv.linked
            enabled: visible

            Label {
                id:label
                color: Theme.primaryColor
                text: qsTr("Linked to Mobile Vikings")
                x: parent.x + Theme.paddingLarge
            }

            Label {
                anchors.top: label.bottom
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                text: qsTr("Press and hold to unlink")
                x: parent.x + Theme.paddingLarge
            }

            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Unlink")
                    onClicked: linkedItem.remorseAction(qsTr("Unlinking"), function() { mv.unlink() })
                }
            }
        }

        ListItem {
            id:wrongPasswordItem
            anchors.top: header.bottom
            contentHeight: Theme.itemSizeSmall
            visible: false
            enabled: visible

            Label {
                id:wrongLabel
                x: parent.x + Theme.paddingLarge
                color: "red"
                text: qsTr("Invalid username or password")
            }
        }

        TextField {
            placeholderText: qsTr("Username")
            label: placeholderText
            anchors.top: wrongPasswordItem.visible ? wrongPasswordItem.bottom : header.bottom
            id: userNameText
            text: ""
            width: parent.width

            onTextChanged: color = Theme.primaryColor

            visible: !mv.linked
            enabled: visible
        }

        TextField {
            placeholderText: qsTr("Password")
            label: placeholderText
            anchors.top: userNameText.bottom
            id: passwordText
            echoMode: TextInput.Password
            text: ""
            width: parent.width

            onTextChanged: color = Theme.primaryColor

            visible: !mv.linked
            enabled: visible
        }

        /*Separator {
            id: separator1
            anchors.top: passwordText.bottom
            width: parent.width
        }*/

        ComboBox {
            id: simList
            anchors.top: mv.linked ? linkedItem.bottom : passwordText.bottom
            width: parent.width
            label: qsTr("Sim Card : ")

            currentIndex: mv.selectedSim + 1

            menu: ContextMenu {
                MenuItem {
                    text: qsTr("None")
                    onClicked: {
                        mv.selectedSim = -1
                    }
                }

                Repeater {
                    model: mv.simCards

                    delegate: MenuItem {
                        text: modelData.msisdn
                        onClicked: {
                            mv.selectedSim = index
                        }
                    }
                }
            }
        }

        /*Separator {
            id: separator2
            anchors.top: simList.bottom
            width: parent.width
        }*/

        /*TextSwitch {
            id: autoUpdate
            anchors.top: separator2.bottom
            text: qsTr("Auto Update")
            checked: true//settings.autoUpdate
            onCheckedChanged:
            {
                //settings.autoUpdate = checked
                updateFreq.enabled = checked
            }
        }

        TextField {
            id: updateFreq
            anchors.top: autoUpdate.bottom
            placeholderText: qsTr("Frequency")
            label: placeholderText
        }*/

        VerticalScrollDecorator {}
    }

    onAccepted:
    {
        if (!mv.linked)
        {
            mv.userName = userNameText.text
            mv.password = passwordText.text
            mv.connect();
        }
        else
            mv.getSimCards(true)
    }

    acceptDestination: ConnectingPage {}

}





