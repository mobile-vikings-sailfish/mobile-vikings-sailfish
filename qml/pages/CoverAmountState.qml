import QtQuick 2.0
import Sailfish.Silica 1.0

Rectangle {
    property double value
    property string prettyValue
    property string icon
    property bool isViking: false

    color: Theme.rgba(Theme.secondaryColor, 0.5)

    Rectangle {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        radius: 3
        color: Theme.rgba(Theme.highlightColor, 0.4)

        width: parent.width * value
    }

    Image {
        id: iconImage
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: 5
        source: icon
        width: 64
        height: width
    }

    Image {
        visible: isViking
        anchors.right: iconImage.left
        anchors.verticalCenter: parent.verticalCenter
        width: iconImage.width * 0.7
        height: width
        source: "qrc:///img/helmet.png"
    }

    Text {
        text: prettyValue
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        font.pointSize: Theme.fontSizeExtraSmall
        color: Theme.primaryColor

        anchors.margins: 5
    }
}
