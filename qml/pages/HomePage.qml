/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.mobile.vikings.mv 1.0

Page {
    id: page

    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        id:col
        width: parent.width
        height: parent.height

        contentHeight: height

        PageHeader {
            title: "Mobile Vikings"
            id: header
        }

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            busy: mv.updating
            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
            MenuItem {
                text: qsTr("Update")
                onClicked: mv.update()
                enabled: !mv.updating && mv.linked
            }
        }

        PushUpMenu {
            busy: mv.updating
            MenuItem {
                text: qsTr("Usage")
                onClicked: {
                    mv.updateUsage()
                    pageStack.push(Qt.resolvedUrl("UsagePage.qml"))
                }
            }
        }

        Connections {
            target: mv
            onSelectedSimChanged:
            {
                if (mv.selectedSim === -1)
                {
                    smsMVProgress.value = 0
                    voiceProgress.value = 0
                    smsProgress.value = 0
                    dataProgress.value = 0
                    timeLeft.value = 0

                    smsMVProgress.prettyValue = 0
                    voiceProgress.prettyValue = 0 + " " + qsTr("Min")
                    smsProgress.prettyValue = 0
                    dataProgress.prettyValue = 0 + " MB"
                    timeLeft.prettyValue = ""
                    creditLeft.prettyValue = 0 + " €";
                }
            }
        }

        function setValues() {
            if (mv.selectedSim === -1)
                return;

            var balance = mv.simCards[mv.selectedSim].balance

            if (balance !== null)
            {
                if (balance.smsMvBundle() !== null)
                {
                    smsMVProgress.value = balance.smsMvBundle().progress
                    smsMVProgress.prettyValue = balance.smsMvBundle().amount
                }
                if (balance.voiceMvBundle() !== null)
                {
                    voiceProgress.value = balance.voiceMvBundle().progress
                    voiceProgress.prettyValue = Math.floor(balance.voiceMvBundle().amount/60) + " " + qsTr("Min")
                }
                if (balance.smsBundle() !== null)
                {
                    smsProgress.value = balance.smsBundle().progress
                    smsProgress.prettyValue = balance.smsBundle().amount
                }
                if (balance.dataBundle() !== null)
                {
                    dataProgress.value = balance.dataBundle().progress
                    dataProgress.prettyValue = Math.floor(balance.dataBundle().amount/1024/1024) + " MB"
                }

                timeLeft.value = balance.daysLeft() / 31.0

                timeLeft.prettyValue = balance.daysLeft() <= 0 ? "" : balance.daysLeft() + " " + qsTr("days")
                creditLeft.prettyValue = balance.creditLeft() + " €";
            }
            else
            {
                smsMVProgress.value = 0
                voiceProgress.value = 0
                smsProgress.value = 0
                dataProgress.value = 0
                timeLeft.value = 0

                smsMVProgress.prettyValue = 0
                voiceProgress.prettyValue = 0 + " " + qsTr("Min")
                smsProgress.prettyValue = 0
                dataProgress.prettyValue = 0 + " MB"
                timeLeft.prettyValue = ""
                creditLeft.prettyValue = 0 + " €";
            }
        }

        Component.onCompleted: setValues()

        Connections {
            target: mv.selectedSim === -1 ? null : mv.simCards[mv.selectedSim]
            onBalanceChanged: {
                col.setValues()
            }
        }

        states: [
            State {
                name: "landscape"
                when: isLandscape
                AnchorChanges {
                    target: timeLeft

                    anchors.horizontalCenter: undefined //parent.horizontalCenter
                    anchors.verticalCenter: col1.verticalCenter
                    anchors.top: undefined //row1.bottom
                    anchors.left: col1.right
                }
                AnchorChanges {
                    target: creditLeft

                    anchors.horizontalCenter: undefined //parent.horizontalCenter
                    anchors.verticalCenter: col2.verticalCenter
                    anchors.top: undefined //row1.bottom
                    anchors.left: col2.right
                }
            }
        ]

        Column {
            id:col1
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            spacing: 3*width/10
            anchors.leftMargin: header.height
        }

        Row {
            id:row1
            anchors.top: header.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: parent.width/10

            AmountState {
                id: smsProgress
                value: 0
                width: isLandscape ? col.height/3 : col.width/3
                height: width
                name: qsTr("SMS")
                prettyValue: "0"
            }

            AmountState {
                id: dataProgress
                value: 0
                width: isLandscape ? col.height/3 : col.width/3
                height: width
                name: qsTr("Data")
                prettyValue: Math.floor(value) + " MB"
            }


            states: [
                State {
                    name: "landscape"
                    when: isLandscape

                    ParentChange {
                        target:smsProgress
                        parent: col1
                        x: 0
                    }
                    ParentChange {
                        target:dataProgress
                        parent: col1
                        x: 0
                    }

                },
                State {
                    name: "portrait"
                    when: !isLandscape

                    ParentChange {
                        target:smsProgress
                        parent: row1
                        y: 0
                    }
                    ParentChange {
                        target:dataProgress
                        parent: row1
                        y: 0
                    }

                }
            ]
        }


        AmountState {
            id:timeLeft
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: row1.bottom
            anchors.margins: Theme.paddingLarge
            width: isLandscape ? parent.height/3 : parent.width/3
            height: width
            name: value <= 0 ? qsTr("Expired") : qsTr("Validity")
            prettyValue: ""
        }

        Column {
            id:col2
            anchors.left: timeLeft.right
            anchors.verticalCenter: parent.verticalCenter
            spacing: 3*width/10
            anchors.leftMargin: Theme.paddingLarge
        }

        Row {
            id:row2
            anchors.top: timeLeft.bottom
            anchors.topMargin: Theme.paddingLarge
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.leftMargin: Theme.paddingLarge
            spacing: parent.width/10

            AmountState {
                id: voiceProgress
                value: 0
                width: isLandscape ? col.height/3 : col.width/3
                height: width
                name: qsTr("Voice")
                prettyValue: value/60 + " " + qsTr("Min")
                isViking: true
            }

            AmountState {
                id: smsMVProgress
                value: 0
                width: isLandscape ? col.height/3 : col.width/3
                height: width
                name: qsTr("SMS")
                prettyValue: value
                isViking: true
            }


            states: [
                State {
                    name: "landscape"
                    when: isLandscape

                    ParentChange {
                        target:voiceProgress
                        parent: col2
                        x: 0
                    }
                    ParentChange {
                        target:smsMVProgress
                        parent: col2
                        x: 0
                    }

                },
                State {
                    name: "portrait"
                    when: !isLandscape

                    ParentChange {
                        target:voiceProgress
                        parent: row2
                        y: 0
                    }
                    ParentChange {
                        target:smsMVProgress
                        parent: row2
                        y: 0
                    }

                }
            ]
        }

        Rectangle {
            property string prettyValue: ""

            id:creditLeft
            radius: width/2
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: row2.bottom
            anchors.topMargin: Theme.paddingLarge
            anchors.leftMargin: Theme.paddingLarge
            width: isLandscape ? parent.height/3 : parent.width/3
            height: width
            color: Theme.rgba(Theme.highlightColor, 0.4)

            Text {
                id: textLabel1
                text: qsTr("Credit")
                font.pointSize: Theme.fontSizeMedium
                color: Theme.primaryColor
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: separator.top
            }

            Rectangle {
                id: separator
                width: parent.width/2
                height: 1
                color: Theme.highlightColor
                anchors.centerIn: parent
            }

            Text {
                id: textLabel2
                text: parent.prettyValue
                font.pointSize: Theme.fontSizeMedium
                color: Theme.primaryColor
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: separator.bottom
            }
        }
    }
}


