import QtQuick 2.0
import Sailfish.Silica 1.0

DatePickerDialog {
    id:dateChooserDialog

    canAccept: true
    width: height

    onAccepted: {
        mv.updateUsage(date)
        pageStack.pop()
    }
}
